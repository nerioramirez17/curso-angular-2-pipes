import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  nombre = "Nerio";

  array = [1,2,3,4,5,6,7,8,9,10];

  PI = Math.PI;

  a = 0.244;

  salario = 1234.5;

  heroe = {
    nombre: "Logan",
    clave: "Wolverine",
    edad: 500,
    direccion:{
      calle: "Primera",
      casa: "19"
    }
  };

  valorDePromesa = new Promise( (resolve,reject) =>{

    setTimeout( () => resolve('llego la data'), 3500);
    
  });

  fecha = new Date();

  video = "OdqiBJBlJoE";

  activar:boolean = true;

  password = "micontrasenia";
}
